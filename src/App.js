import './App.scss';
import Button1 from'./components/Button1/Button1.js';
import Button2 from'./components/Button2/Button2.js';
import Button3 from'./components/Button3/Button3.js';
import Button4 from'./components/Button4/Button4.js';

function App() {
  return (
    <div className="App">
      <Button1></Button1>
      <Button2></Button2>
      <Button3></Button3>
      <Button4></Button4>
    </div>
  );
}

export default App;
